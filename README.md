# Running your own gitlab runner

## Create EC2 machine

Create new EC2 machine with following options:

- Choose AMI: `Ubuntu Server 20.04 LTS`
- Choose Instance Type: any, but staring with `t2.micro`
- Configure Instance: leave defaults
- Add storage
  - Root volume, 64GB, General Purpose SSD, Delete on Termination ✅
  - EBS, 4GB, General Purpose SSD, Delete on Termination ✅
- Add Tags:
  - Name: `gitlab-runner`
- Configure Security Group:
  - ✅ Create a new security group
  - Security group name: `gitlab-runner`
  - Description: `Security group for gitlab-runner`
  - Rules:
    - `SSH`, `TCP`, Port `22`, Source Custom `0.0.0.0/0`

After the instance has launched, take a note of the public IP of the cotainer and SSH into it!

Then follow instructions on [deployment-configuration/docs/swap.md](https://gitlab.com/greenlogistics/deployment-configuration/-/blob/master/docs/swap.md) to enable swap.

## System setup

```shell script
# Update & upgrade system
sudo apt-get -y update
sudo apt-get -y upgrade

# Install requirements
sudo apt-get install -y curl git

# Install docker using convenience script
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh

# Install docker-compose
sudo curl -L "https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# Clone this repository
sudo git clone https://gitlab.com/greenlogistics-software/gitlab-runner /usr/src/gitlab-runner

# Reboot the system
sudo reboot
```

## Runner configuration

Now, we need registration token from GitLab.

Navigate to `Groups > [group name] > Settings > CI/CD` and expand `Runners` section. Take a note of `registration token` on the left side.

Then execute `./scripts/registers.sh` which will ask for the token:

```shell script
cd /usr/src/gitlab-runner
sudo ./scripts/register.sh
```

## Start the runner

If previous steps were successful, all you need to do is to start the runner:

```shell script
cd /usr/src/gitlab-runner
sudo docker-compose up -d
```
