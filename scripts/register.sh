#!/usr/bin/env sh

if [ "${REGISTRATION_TOKEN}" = "" ]; then
  read -p "Registration token: " REGISTRATION_TOKEN
fi

# Register docker runner
docker-compose run --rm gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "${REGISTRATION_TOKEN}" \
  --tag-list "docker,ec2,aws" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected" \
  --executor "docker" \
  --docker-image docker:latest \
  --docker-volumes "/var/run/docker.sock:/var/run/docker.sock"

# Ensure concurrenry
docker-compose run --rm --entrypoint '' gitlab-runner \
  sed -i 's/^concurrent = .*$/concurrent = 5/g' /etc/gitlab-runner/config.toml
